package core.services.mail;

import core.services.interfaces.mail.MailContentBuilder;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.Map;
import java.util.Optional;

@Service
public class FreemarkerContentBuilder implements MailContentBuilder {

    private Configuration freemarkerConfig;

    @Autowired
    public FreemarkerContentBuilder(@Qualifier("freemarker")Configuration freemarkerConfig) {
        this.freemarkerConfig = freemarkerConfig;
    }

    @Override
    public Optional<String> createHTML(Map<String, Object> dataModel, String template) {
        try {
            Template mailTemplate = freemarkerConfig.getTemplate(template);
            return Optional.of(FreeMarkerTemplateUtils.processTemplateIntoString(mailTemplate, dataModel));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
