package core.services.mail;

import core.commands.VoidResult;
import core.commands.mail.SendWelcomeMailCommand;
import core.services.interfaces.mail.EmailClient;
import core.services.interfaces.mail.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@PropertySource("${classpath:mail-config.properties}")
public class DefaultEmailService implements EmailService {
    private static final String MAIL_TEMPLATE_NAME = "mail-template.ftl";

    private static final String MODEL_MSG_TEXT = "msgText";
    private static final String MODEL_USER_FIRST_NAME = "userFirstName";
    private static final String MODEL_USER_LAST_NAME = "userLastName";

    @Value("${user.mail.from: arkturmail@gmail.com}")
    private String fromEmail;
    @Value("${mail.body.message}")
    private String messageText;
    @Value("${mail.subject.message}")
    private String subjectText;

    private EmailClient emailClient;

    @Autowired
    public DefaultEmailService(EmailClient emailClient) {
        this.emailClient = emailClient;
    }

    @Override
    public VoidResult sendRegistrationConfirmEmail(SendWelcomeMailCommand command) {
        Map<String, Object> contentModel = new HashMap<>();
        contentModel.put(MODEL_USER_FIRST_NAME, command.getFirstName());
        contentModel.put(MODEL_USER_LAST_NAME, command.getLastName());
        contentModel.put(MODEL_MSG_TEXT, messageText);

        EmailData emailSendData = new EmailData();
        emailSendData.setTo(command.getEmail());
        emailSendData.setFrom(fromEmail);
        emailSendData.setSubject(subjectText);
        emailSendData.setTemplateName(MAIL_TEMPLATE_NAME);
        emailSendData.setContentModel(contentModel);

        emailClient.prepareAndSendHTMLAsync(emailSendData);
        return new VoidResult();
    }
}
