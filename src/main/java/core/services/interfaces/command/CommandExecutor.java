package core.services.interfaces.command;

import core.commands.DomainCommand;
import core.operationresult.ProcessingResult;

public interface CommandExecutor {

    <T extends ProcessingResult> T execute(DomainCommand<T> domainCommand);

}
