package core.services.interfaces.command;

import core.commands.DomainCommand;
import core.operationresult.ProcessingResult;

public interface DomainCommandHandler<C extends DomainCommand, R extends ProcessingResult> {

    R execute(C command);

    Class getCommandType();

}
