package core.services.interfaces.mail;

import core.services.mail.EmailData;

public interface EmailClient {

    void prepareAndSendHTML(EmailData emailData);

    void prepareAndSendHTMLAsync(EmailData emailData);

    void send(EmailData emailData, String content);

    void sendAsync(EmailData emailData, String content);
}
