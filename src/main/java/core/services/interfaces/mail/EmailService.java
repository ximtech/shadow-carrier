package core.services.interfaces.mail;

import core.commands.VoidResult;
import core.commands.mail.SendWelcomeMailCommand;

public interface EmailService {

    VoidResult sendRegistrationConfirmEmail(SendWelcomeMailCommand command);
}
