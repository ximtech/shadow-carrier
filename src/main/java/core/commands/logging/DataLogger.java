package core.commands.logging;

import core.operationresult.ResponseMessage;
import core.operationresult.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.StringJoiner;
import java.util.stream.Stream;

public class DataLogger {
    private static final Logger LOGGER = LogManager.getLogger(DataLogger.class);

    private static final String[] SCHEDULER_PARAMS = { "core.pool.size=", "max.pool.size=", "keep.alive.time=", "queue.max.size=" };

    public static void logErrorMessage(Exception exception, ResponseMessage responseMessage) {
        logException(responseMessage, exception.getClass().getSimpleName());
        exception.printStackTrace();
    }

    public static void logRequestStart(Object request) {
        LOGGER.info(String.format("Started request %s", request.toString()));
    }

    public static void logRequestEnd(Object request, long endTime) {
        String className = request.getClass().getSimpleName();
        LOGGER.info(String.format("Request with name: %s finished in %d ms", className, endTime));
    }

    public static void logMessageReceivingFromQueue(Object request, String queueName) {
        String className = request.getClass().getSimpleName();
        LOGGER.info("Receiving request={} from queue={}",className, queueName);
    }

    public static void logSchedulerParameters(String schedulerName, int... values) {
        String prefix = "Creating " + schedulerName + " scheduler: ";
        StringJoiner joiner = new StringJoiner(", ", prefix, "");
        Stream.iterate(0, i -> i + 1)
                .limit(SCHEDULER_PARAMS.length)
                .forEach((Integer i) -> joiner.add(SCHEDULER_PARAMS[i] + values[i]));
        LOGGER.info(joiner.toString());
    }

    private static void logException(ResponseMessage responseMessage, String exceptionClassName) {
        Status responseStatus = responseMessage.getStatus();
        String message = responseMessage.getMessage();
        LOGGER.error(String.format("%1$s. Status: %2$s Message: %3$s", exceptionClassName, responseStatus, message));
    }
}
