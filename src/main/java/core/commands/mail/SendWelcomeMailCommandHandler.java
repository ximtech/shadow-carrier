package core.commands.mail;

import core.commands.VoidResult;
import core.services.interfaces.command.DomainCommandHandler;
import core.services.interfaces.mail.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class SendWelcomeMailCommandHandler implements DomainCommandHandler<SendWelcomeMailCommand, VoidResult> {

	private EmailService emailService;

	@Autowired
	public SendWelcomeMailCommandHandler(EmailService emailService) {
		this.emailService = emailService;
	}

	@Override
	public VoidResult execute(SendWelcomeMailCommand command) {
		return emailService.sendRegistrationConfirmEmail(command);
	}

	@Override
	public Class getCommandType() {
		return SendWelcomeMailCommand.class;
	}
	
}
