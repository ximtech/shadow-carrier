package core.commands.mail;

import core.commands.DomainCommand;
import core.commands.VoidResult;

import java.io.Serializable;
import java.util.StringJoiner;

public class SendWelcomeMailCommand implements DomainCommand<VoidResult>, Serializable {
    private String firstName;
    private String lastName;
    private String email;

    public SendWelcomeMailCommand() {}

    public SendWelcomeMailCommand(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "SendWelcomeMailCommand {", "}")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("email='" + email + "'")
                .toString();
    }
}
