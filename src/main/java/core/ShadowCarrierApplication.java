package core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class ShadowCarrierApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShadowCarrierApplication.class, args);
    }
}
