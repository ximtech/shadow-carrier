package core.mq;

import core.commands.logging.DataLogger;
import core.commands.mail.SendWelcomeMailCommand;
import core.services.interfaces.command.CommandExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import javax.jms.Message;
import javax.jms.Session;

import static core.ActiveMQConfig.MESSAGE_QUEUE;

@Component
public class MailMessageConsumer {//TODO implement scheduler

    private CommandExecutor commandExecutor;

    @Autowired
    public MailMessageConsumer(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    @JmsListener(destination = MESSAGE_QUEUE)
    public void receiveMessage(@Payload SendWelcomeMailCommand command,
                               @Headers MessageHeaders headers,
                               Message message, Session session) {
        DataLogger.logMessageReceivingFromQueue(command, MESSAGE_QUEUE);
        Mono.just(commandExecutor.execute(command)).subscribe();
    }
}
