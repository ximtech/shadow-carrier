# Project

Micro service for learning and testing purposes.
Specially created for JavaGuru 3

#Description

Project main purpose is to handle customer registration via rest
Validate user data, save to db, send to mail sender micro service with jms and then send 
welcome mail to the customer.

The project contains two parts:
1. Micro service with db, validation and processing message to jms
2. Shadow carrie main purpose is to format email message, fill with customer data
and asynchronous send mail message.

#Usage

1. clone first project from: git clone git@bitbucket.org:ximtech/javaguru3.git
2. clone second project from: git clone git@bitbucket.org:ximtech/shadow-carrier.git
3. download activemq from: http://activemq.apache.org/download.html
4. start mq with command: "activemq start", from [activemq_install_dir]/bin in cmd
5. Open the administrative interface: http://127.0.0.1:8161/admin/
6. Login: admin, Passwort: admin
7. Navigate to "Queues" -> Add a queue name and click create("outbound.endpoint")
8. Launch projects
9. In Postman: http://localhost:8090/api/registerNewUser
 
 And params like: 
 * firstName -> AAAAA
 * lastName -> BBBBB
 * password -> 3120456Abcd
 * email -> "email for receiving welcome mail"
   
   
   
   